import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, Platform } from 'ionic-angular';

@IonicPage() @Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html'
})
export class PerfilPage {

  constructor(
    public navCtrl: NavController, 
    public viewCtrl: ViewController,
    public platform: Platform,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreferenciaPage');
  }

  onDismiss(data?: any) {
    this.viewCtrl.dismiss(data);
  }

  onConfirm() {
   this.onDismiss();
  }

  onLogin(isLogin: boolean): void {
    console.log(isLogin);
  }

}

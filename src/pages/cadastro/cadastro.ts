import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  constructor(
    public navCtrl: NavController, 
    public modalCtrl: ModalController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroPage');
  }

  onPresentConfigs(): void {
    let modal = this.modalCtrl.create('ConfiguracaoPage', {});
    modal.present();
    modal.onWillDismiss((data: any) => console.log(data));
  }

  onSubmit(): void {
    this.navCtrl.popToRoot();
  }

}

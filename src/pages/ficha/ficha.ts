import { Component } from '@angular/core';
import { IonicPage, NavController, PopoverController, NavParams } from 'ionic-angular';

import { Item } from '../../interfaces';

@IonicPage()
@Component({
  selector: 'page-ficha',
  templateUrl: 'ficha.html',
})
export class FichaPage {
  listItem: Item[] = [
    { $key: '1', descricao: 'Encerrar', status: true, icone: null },
    { $key: '2', descricao: 'Reabrir', status: true, icone: null }
  ]
  segment = 'notificacao';
  tab1Root = 'DetalhePage';
  tab2Root = 'HistoricoPage';
  tab3Root = 'ChatPage';
  tab4Root = 'NotificacaoPage';
  
  constructor(
    public navCtrl: NavController, 
    public popoverCtrl: PopoverController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FichaPage');
  }

  onPresentOptions(event: Event): void {
    let popover = this.popoverCtrl.create('OpcoesPage', { items: this.listItem, title: 'Opções de Ordenação' });
    popover.present({ ev: event }).then(data => console.log(data)).catch(erro => console.log(erro));
    popover.onDidDismiss((data: Item) => console.log(JSON.stringify(data)));
  }
}

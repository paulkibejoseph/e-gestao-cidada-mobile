import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

@IonicPage() @Component({
  selector: 'page-compartilhar',
  templateUrl: 'compartilhar.html'
})
export class CompartilharPage {

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompartilharPage');
  }

  onDismiss(data?: any) {
    this.viewCtrl.dismiss(data);
  }

}

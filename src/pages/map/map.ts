import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, ModalController, Platform } from 'ionic-angular';

import { AppData } from '../../providers/app-data';

declare var google: any;

@IonicPage() @Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {

  @ViewChild('mapCanvas') mapElement: ElementRef;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public appData: AppData) {
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      this.initializeMap();
    });
  }

  initializeMap() {
    this.appData.getMap().subscribe((mapData: any) => {
      let pin = 'assets/img/map-marker-red.png';
      let minZoomLevel = 15;
      let center = mapData.find((d: any) => d.center);
      let mapEle = this.mapElement.nativeElement;

      let map = new google.maps.Map(mapEle, {
        center: center,
        zoom: minZoomLevel
      });

      mapData.forEach((markerData: any) => {
        let marker = new google.maps.Marker({
          position: markerData,
          map: map,
          title: markerData.name
        });
        marker.setIcon(markerData.center ? 'assets/img/map-marker-indigo.png' : pin);

        marker.addListener('click', () => {
          this.navCtrl.push("TabsPage", {});
        });
      });

      google.maps.event.addListenerOnce(map, 'idle', () => {
        mapEle.classList.add('show-map');
      });
    });

  }

  onOpenNotificacao(): void {
    // let modal = this.modalCtrl.create('NotificacaoPage', { isModal: true });
    // modal.present();
    // modal.onWillDismiss((data: any) => console.log(data));
  }

  onOpenCamera(): void {
    this.navCtrl.push('CameraPage');
  }

  onOpenGaleria(): void {
    this.navCtrl.push('GaleriaPage');
  }

  onOpenCadastro(): void {
    this.navCtrl.push('CadastroPage');
  }

}

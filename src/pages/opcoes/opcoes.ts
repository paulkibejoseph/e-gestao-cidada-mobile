import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

import { Item } from '../../interfaces';

@IonicPage() @Component({
  selector: 'page-opcoes',
  templateUrl: 'opcoes.html'
})
export class OpcoesPage {

  items: Item[] = [];
  title: string = 'Opções';

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.items = this.navParams.data.items || [];
    this.title = this.navParams.data.title || 'Opções';
  }

  onSelect(item: Item): void {
    this.dismiss(item)
  }

  private dismiss(item: Item) {
    this.viewCtrl.dismiss(item);
  }

}

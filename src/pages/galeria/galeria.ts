import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-galeria',
  templateUrl: 'galeria.html',
})
export class GaleriaPage {

  list: number[] = [];
  rows: number[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
    this.list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    this.rows = Array.from(Array(Math.ceil((this.list).length / 2)).keys());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GaleriaPage');
  }

  onOpenCadastro(): void {
    this.navCtrl.push('CadastroPage');
  }

}

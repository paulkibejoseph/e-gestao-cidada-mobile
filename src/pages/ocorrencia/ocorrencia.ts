import { Component } from '@angular/core';
import { IonicPage, NavController,PopoverController,  NavParams } from 'ionic-angular';

import { Item } from '../../interfaces';

@IonicPage()
@Component({
  selector: 'page-ocorrencia',
  templateUrl: 'ocorrencia.html',
})
export class OcorrenciaPage {
  listItem: Item[] = [
    { $key: '1', descricao: 'Por Data', status: true, icone: null },
    { $key: '2', descricao: 'Por Categoria', status: true, icone: null },
    { $key: '3', descricao: 'Por Protestometro', status: true, icone: null }
  ]
  filter: any = { query: '', placeholder: 'Pesquisa' };

  list: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  
  constructor(
    public navCtrl: NavController, 
    public popoverCtrl: PopoverController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OcorrenciaPage');
  }

  onPresentOptions(event: Event): void {
    let popover = this.popoverCtrl.create('OpcoesPage', { items: this.listItem, title: 'Opções de Ordenação' });
    popover.present({ ev: event }).then(data => console.log(data)).catch(erro => console.log(erro));
    popover.onDidDismiss((data: Item) => console.log(JSON.stringify(data)));
  }

  onSearch(): void {
    // TODO fitrar listAdvogado
  }

  onOpenDetalhes(): void {
    this.navCtrl.push("TabsPage", {});
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreferenciaPage } from './preferencia';

@NgModule({
  declarations: [
    PreferenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(PreferenciaPage),
  ],
})
export class PreferenciaPageModule {}

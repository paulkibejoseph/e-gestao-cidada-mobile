import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage() @Component({
  selector: 'page-sobre',
  templateUrl: 'sobre.html'
})
export class SobrePage {

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public platform: Platform,
    public iab: InAppBrowser) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SobrePage');
  }

  onDismiss(data?: any) {
    this.viewCtrl.dismiss(data);
  }

  onGetIntoContact(): void {
    window.open('mailto:' + 'olharcidadao@olharcidadao.com.br');
  }

  onInAppBrowser(): void {
    const browser = this.iab.create(`https://www.facebook.com/olharcidadao`, '_blank');
    browser.show();
  }

}

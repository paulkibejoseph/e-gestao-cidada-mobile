import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import { Database, Mapa } from "../interfaces";

@Injectable()
export class AppData {
    database: Database;

    constructor(public http: Http) { }

    getMap(): Observable<Mapa[]> {
        return this.load().map((data: any) => <Mapa[]>data.map);
    }

    load(): Observable<Database> {
        if (this.database) {
            return Observable.of(this.database);
        } else {
            return this.http.get('assets/data/data.json').map(this.processData, this);
        }
    }

    private processData(data: any) {
        return data.json();
    }
}
import { Component, ViewChild } from '@angular/core';
import { Events, ModalController, MenuController, AlertController, Nav, Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';

import { AppData } from '../providers/app-data';
import { UserData } from '../providers/user-data';

export interface PageInterface {
  title: string;
  name: string;
  icon: string;
  index?: number;
  isRoot?: boolean;
}

@Component({
  templateUrl: 'app.html'
})
export class ConferenceApp {
  @ViewChild(Nav) nav: Nav;

  appPages: PageInterface[] = [
    { title: 'Mapa', name: 'MapPage', icon: 'map', index: 0, isRoot: true },
    { title: 'Ocorrências', name: 'OcorrenciaPage', icon: 'bonfire', index: 1, isRoot: true }
  ];
  userPages: PageInterface[] = [
    { title: 'Perfil', name: 'PerfilPage', icon: 'person' },
    { title: 'Preferências', name: 'PreferenciaPage', icon: 'settings' },
    { title: 'Logout', name: 'LogoutPage', icon: 'log-out' }
  ];
  miscellaneousPages: PageInterface[] = [
    { title: 'Ajuda', name: 'AjudaPage', icon: 'help' },
    { title: 'Sobre', name: 'SobrePage', icon: 'information-circle' },
    { title: 'Compartilhar', name: 'CompartilharPage', icon: 'share' }
  ];

  rootPage: any;

  constructor(
    public events: Events,
    public modalCtrl: ModalController,
    public menu: MenuController,
    public alertCtrl: AlertController,
    public platform: Platform,
    public storage: Storage,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public appData: AppData,
    public userData: UserData) {
    this.init();
  }

  onOpenPage(page: PageInterface) {
    if (page.name == 'MapPage') {
      this.openRootPage(page.name);
    } else if (page.name == 'OcorrenciaPage') {
      this.nav.getActive().getNav().push('OcorrenciaPage', page);
    } else if (page.name == 'LogoutPage') {
      this.onLogout();
    } else {
      this.openModalPage(page.name, page);
    }
  }

  isActive(page: PageInterface) {
    return (this.nav.getActive() && this.nav.getActive().name === page.name);
  }

  private init() {
    this.storage.get('hasSeenTutorial')
      .then((hasSeenTutorial: boolean) => this.rootPage = hasSeenTutorial ? 'MapPage' : 'TutorialPage')
      .then(_ => this.platformReady())
      .then(_ => this.appData.load())
      .catch((error: any) => console.log(error));
  }

  private platformReady() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    }).catch((error: any) => console.log(error));
  }

  private openRootPage(page: string) {
    this.nav.setRoot(page, {}, { animate: true, direction: 'forward' })
      .catch((error: any) => console.log(error));
  }

  private openModalPage(page: string, data: any) {
    let modal = this.modalCtrl.create(page, data, {});
    modal.present().catch((error: any) => console.log(error));
    modal.onWillDismiss((data: any) => console.log(data));
  }

  private onLogout(): void {
    let confirm = this.alertCtrl.create({
      title: 'Logout',
      message: `Deseja realizar o logout ?`,
      buttons: [
        { text: 'Não', handler: () => console.log('Não clicked') },
        { text: 'Sim', handler: () => console.log('Sim clicked') }
      ]
    });
    confirm.present();
  }

}
